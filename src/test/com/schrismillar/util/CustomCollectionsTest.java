package com.schrismillar.util;

import static com.schrismillar.util.CustomCollections.list;
import static org.testng.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class CustomCollectionsTest {

	private List<String> expectedList;

	@BeforeMethod
	public void SetUp() {
		expectedList = new ArrayList<String>();
		expectedList.add("Hello");
		expectedList.add("World");
		expectedList.add("!");
	}
	
	@Test
	public void testList() {	
		List<String> actualList = list("Hello", "World", "!");

		assertEquals(expectedList, actualList);
	}
}
