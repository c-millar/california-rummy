package com.schrismillar.game;

import static org.testng.Assert.assertEquals;
import static org.mockito.Mockito.when;
import static com.schrismillar.util.CustomCollections.list;

import java.util.List;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.schrismillar.game.player.Player;

public class GameTest {
	private static final String PLAYER_ONE_NAME = "Player One Name";
	private static final String PLAYER_TWO_NAME = "Player Two Name";
	private static final String PLAYER_THREE_NAME = "Player Three Name";
	
	@Mock private Player playerOne;
	@Mock private Player playerTwo;
	@Mock private Player playerThree;

	private List<Player> players;
	
	private Game game;
	
	@BeforeMethod
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		
		players = list(playerOne, playerTwo, playerThree);
		
		when(playerOne.getName()).thenReturn(PLAYER_ONE_NAME);
		when(playerTwo.getName()).thenReturn(PLAYER_TWO_NAME);
		when(playerThree.getName()).thenReturn(PLAYER_THREE_NAME);
		
		game = Game.gameWithPlayers(players);
	}
	
	@Test
	public void testGetPlayerNames() {
		List<String> expectedNames = list(PLAYER_ONE_NAME, PLAYER_TWO_NAME, PLAYER_THREE_NAME);
		
		List<String> actualNames = game.getPlayerNames();
		
		assertEquals(expectedNames, actualNames);
		
	}
}
