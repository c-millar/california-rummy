package com.schrismillar.scorekeeper;

import static org.testng.Assert.assertEquals;
import static org.mockito.Mockito.when;
import static com.schrismillar.util.CustomCollections.list;

import java.util.List;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;

import com.schrismillar.cards.Card;
import com.schrismillar.cards.rank.Rank;
import com.schrismillar.cards.rank.RankEnum;
import com.schrismillar.cards.suit.DiamondSuit;
import com.schrismillar.cards.suit.HeartSuit;
import com.schrismillar.game.hand.Hand;

public class HandCounterTest {
	private static final int ZERO = 0;

	private static final List<Card> EMPTY_LIST = list();

	@Mock private Hand handWithZeroCards;
	@Mock private Hand handWithTwentyFivePoints;
	
	private HandCounter handCounter;

	private List<Card> listOfCardsInHand;


	@BeforeMethod
	public void SetUp() {
		MockitoAnnotations.initMocks(this);

		Card ace = new Card(new DiamondSuit(), new Rank(RankEnum.ACE));
		Card five = new Card(new HeartSuit(), new Rank(RankEnum.FIVE));
		listOfCardsInHand = list(ace, five);
		
		when(handWithZeroCards.getCardsInHand()).thenReturn(EMPTY_LIST);
		when(handWithTwentyFivePoints.getCardsInHand()).thenReturn(listOfCardsInHand);
		
		handCounter = new HandCounter();
	}
	
	@Test
	public void testCountHandOnHandWithZeroCards() {
		int sum = handCounter.countHand(handWithZeroCards);
		
		assertEquals(ZERO, sum);
	}
	
	@Test
	public void testCountHandOnHandWithTwentyFivePoints() {
		int sum = handCounter.countHand(handWithTwentyFivePoints);
		
		assertEquals(25, sum);
	}
}
