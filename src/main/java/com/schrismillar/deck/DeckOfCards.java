package com.schrismillar.deck;

import static com.schrismillar.util.CustomCollections.list;

import java.util.List;
import java.util.Random;

import com.schrismillar.cards.Card;
import com.schrismillar.cards.rank.Rank;
import com.schrismillar.cards.suit.ClubSuit;
import com.schrismillar.cards.suit.DiamondSuit;
import com.schrismillar.cards.suit.HeartSuit;
import com.schrismillar.cards.suit.SpadeSuit;
import com.schrismillar.cards.suit.Suit;


public class DeckOfCards {
	private List<Card> cardsInDeck;
	
	private DeckOfCards() {
		cardsInDeck = list();
	}
	
	public static DeckOfCards DeckOfCardsFactory() {
		DeckOfCards deck = new DeckOfCards();
		
		add53CardsToDeck(deck);
		
		return deck;
	}

	private static void add53CardsToDeck(DeckOfCards deck) {
		deck.addCardsToDeck(listOfCardsOfSuit(HeartSuit.instance()));
		deck.addCardsToDeck(listOfCardsOfSuit(DiamondSuit.instance()));
		deck.addCardsToDeck(listOfCardsOfSuit(SpadeSuit.instance()));
		deck.addCardsToDeck(listOfCardsOfSuit(ClubSuit.instance()));
		deck.addCardToDeck(Card.jokerCard());
	}
	
	private void addCardToDeck(Card card) {
		cardsInDeck.add(card);
	}

	private void addCardsToDeck(List<Card> listOfCardsToAdd) {
			cardsInDeck.addAll(listOfCardsToAdd);
	}
	
	private static List<Card> listOfCardsOfSuit(Suit suit) {
		List<Card> cards = list();
		
		for (Rank rank : Rank.RANK_LIST) {
			if (rank.isNotJoker()) {
				cards.add(new Card(suit, rank));
			}
		}
	
		return cards;
	} 
	
	public int getSize() {
		return cardsInDeck.size();
	}
	
	public List<String> getSymbolNamesOfCardsInDeck() {
		List<String> symbols = list();
		
		for (Card card : cardsInDeck) {
			symbols.add(card.getSymbolName());
		}
		
		return symbols;
	}
	
	public DeckOfCards shuffleDeck(int numTimesToShuffle) {
		for (int i = 0; i < numTimesToShuffle; i++) {
			shuffleList();			
		}
		
		return this;
	}

	private void shuffleList() {
		Random rand = new Random();
		
		int randomIndexToSwapWith = rand.nextInt(getSize());
		for (int i = 0; i < getSize(); i++) {
			Card tempCopyToSwap = cardsInDeck.get(randomIndexToSwapWith);
			cardsInDeck.set(randomIndexToSwapWith, cardsInDeck.get(i));
			cardsInDeck.set(i, tempCopyToSwap);
			
			randomIndexToSwapWith = rand.nextInt(getSize());
		}
	}
	

}
