package com.schrismillar.main;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;

import com.schrismillar.deck.DeckOfCards;
import com.schrismillar.util.ListPrinter;

public class Main {
	public static void main(String[] args) {
		DeckOfCards deck = DeckOfCards.DeckOfCardsFactory();
		
		System.out.println("Deck is of size " + deck.getSize());
		
		System.out.println("\nUNSHUFFLED DECK");
		try {
			ListPrinter.getListPrinter(new PrintStream(new File("C:\\Users\\Chris\\Desktop\\rummy output.txt"))).printList(deck.getSymbolNamesOfCardsInDeck());
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		deck.shuffleDeck(5);
		System.out.println("\nSHUFFLED DECK");
		ListPrinter.getDefaultListPrinter().printList(deck.getSymbolNamesOfCardsInDeck());
	}
	
}
