package com.schrismillar.game;

import static com.schrismillar.util.CustomCollections.list;

import java.util.List;

import com.schrismillar.game.player.Player;

public class Game {
	private List<Player> players;
	
	private Game() {
		players = list();
	}

	public static Game gameWithPlayers(List<Player> players) {
		Game game = new Game();
		
		game.setPlayers(players);
		
		return game;
	}
	
	private void setPlayers(List<Player> playersToAdd) {
		players.clear();
		players.addAll(playersToAdd);
	}
	
	public List<String> getPlayerNames() {
		List<String> playerNames = list();
		
		for (Player player : players) {
			playerNames.add(player.getName());
		}
		
		return playerNames;
	}
	
}
