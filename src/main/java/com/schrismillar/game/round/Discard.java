package com.schrismillar.game.round;

public class Discard implements Objective {
	private boolean allowed;
	
	private Discard(boolean allowed) {
		this.allowed = allowed;
	}
	
	public static Discard notAllowed() {
		return new Discard(false);
	}
	
	public static Discard allowed() {
		return new Discard(true);
	}
}
