package com.schrismillar.game.round;

public class Straight implements Objective {
	private int length;
	
	public Straight(int length) {
		this.length = length;
	}
	
	public static Straight ofLength(int length) {
		return new Straight(length);
	}
}
