package com.schrismillar.game.round;

public class Set implements Objective {
	private int size;
	
	public Set(int size) {
		this.size = size;
	}
	
	public static Set ofSize(int size) {
		return new Set(size);
	}
}
