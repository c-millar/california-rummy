package com.schrismillar.game.hand;

import static com.schrismillar.util.CustomCollections.list;

import java.util.List;

import com.schrismillar.cards.Card;

public class Hand {
	public List<Card> hand;
	
	public Hand() {
		hand = list();
	}

	public void addCardToHand(Card card) {
		hand.add(card);
	}
	
	public List<Card> getCardsInHand() {
		return hand;
	}
	
	public int getNumberOfCardsInHand() {
		return hand.size();
	}
}
