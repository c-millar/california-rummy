package com.schrismillar.cards.suit;

public class ClubSuit extends Suit {
	private static ClubSuit instance;
	
	
	public ClubSuit() {
		super(SuitEnum.CLUBS, "Clubs", "C");
	}
	
	public static ClubSuit buildSuit() {
		return new ClubSuit();
	}
	
	public static ClubSuit instance() {
		if (instance == null) {
			return buildSuit();
		}
		
		return instance;
	}
}
