package com.schrismillar.cards.suit;

public class WildSuit extends Suit {
	private static WildSuit instance;
	
	public WildSuit() {
		super(SuitEnum.WILD, "No Suit", "*");
	}

	public static WildSuit buildSuit() {
		return new WildSuit();
	}
	
	public static WildSuit instance() {
		if (instance == null) {
			return buildSuit();
		}
		
		return instance;
	}
	
	
}
