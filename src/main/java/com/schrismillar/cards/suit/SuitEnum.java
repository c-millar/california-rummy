package com.schrismillar.cards.suit;

public enum SuitEnum {
	HEARTS, DIAMONDS, SPADES, CLUBS, WILD;
}
