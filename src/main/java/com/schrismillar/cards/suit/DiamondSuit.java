package com.schrismillar.cards.suit;

public class DiamondSuit extends Suit {
	private static DiamondSuit instance;
	
	public DiamondSuit() {
		super(SuitEnum.DIAMONDS, "Diamonds", "D");
	}

	public static DiamondSuit buildSuit() {
		return new DiamondSuit();
	}
	
	public static DiamondSuit instance() {
		if (instance == null) {
			return buildSuit();
		}
		
		return instance;
	}
}
