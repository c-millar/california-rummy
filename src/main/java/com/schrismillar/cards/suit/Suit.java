package com.schrismillar.cards.suit;

public abstract class Suit {
	private SuitEnum suit;
	private String name;
	private String symbol;
	
	public Suit(SuitEnum suit, String name, String symbol) {
		this.suit = suit;
		this.name = name;
		this.symbol = symbol;
	}

	public static Suit buildSuit() {
		return null;
	}
	
	public static Suit instance() {
		return null;
	}
	
	public SuitEnum getSuit() {
		return suit;
	}
	
	public String getName() {
		return name;
	}
	
	public String getSymbol() {
		return symbol;
	}
	
}
