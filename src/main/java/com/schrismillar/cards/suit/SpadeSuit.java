package com.schrismillar.cards.suit;

public class SpadeSuit extends Suit {
	private static SpadeSuit instance;
	
	public SpadeSuit() {
		super(SuitEnum.SPADES, "Spades", "S");
	}

	public static SpadeSuit buildSuit() {
		return new SpadeSuit();
	}
	
	public static SpadeSuit instance() {
		if (instance == null) {
			return buildSuit();
		}
		
		return instance;
	}
}
