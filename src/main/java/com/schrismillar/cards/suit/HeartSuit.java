package com.schrismillar.cards.suit;

public class HeartSuit extends Suit {
	private static HeartSuit instance;
	
	public HeartSuit() {
		super(SuitEnum.HEARTS, "Hearts", "H");
	}

	public static HeartSuit buildSuit() {
		return new HeartSuit();
	}
	
	public static HeartSuit instance() {
		if (instance == null) {
			return buildSuit();
		}
		
		return instance;
	}
}
