package com.schrismillar.cards.rank;

import java.util.Arrays;
import java.util.List;

public class Rank {
	public static final List<Rank> RANK_LIST =  Arrays.asList(
			new Rank(RankEnum.TWO), new Rank(RankEnum.THREE), new Rank(RankEnum.FOUR),
			new Rank(RankEnum.FIVE), new Rank(RankEnum.SIX), new Rank(RankEnum.SEVEN),
			new Rank(RankEnum.EIGHT), new Rank(RankEnum.NINE), new Rank(RankEnum.TEN),
			new Rank(RankEnum.JACK), new Rank(RankEnum.QUEEN), new Rank(RankEnum.KING),
			new Rank(RankEnum.ACE), new Rank(RankEnum.JOKER));
	
	private RankEnum rank;
	private String symbol;
	
	public Rank(RankEnum rank) {
		this.rank = rank;
		this.symbol = decideSymbolFromRankEnum();
	}
	
	private String decideSymbolFromRankEnum() {
		switch (rank) {
			case TWO : 
				return "2";
			case THREE:
				return "3";
			case FOUR:
				return "4";
			case FIVE:
				return "5";
			case SIX:
				return "6";
			case SEVEN:
				return "7";
			case EIGHT:
				return "8";
			case NINE:
				return "9";
			case TEN:
				return "10";
			case JACK:
				return "J";
			case QUEEN:
				return "Q";
			case KING:
				return "K";
			case ACE:
				return "A";
			case JOKER:
				return "$";
			default:
				return "ERROR";
		}
	}
	
	public RankEnum getRank() {
		return rank;
	}
	
	public String getSymbol() {
		return symbol;
	}

	public boolean isNotJoker() {
		if (rank == RankEnum.JOKER) {
			return false;
		}
		
		return true;
	}
	
	public static Rank buildJokerRank() {
		return new Rank(RankEnum.JOKER);
	}
}
