package com.schrismillar.cards;

import com.schrismillar.cards.rank.Rank;
import com.schrismillar.cards.suit.Suit;
import com.schrismillar.cards.suit.WildSuit;



public class Card {
	private Suit suit;
	private Rank rank;
	private String name;
	private int points;
	
	public Card(Suit suit, Rank rank) {
		this.suit = suit;
		this.rank = rank;
		
		this.name = rank.getSymbol() + " of " + suit.getName();
		this.points = determinePointsFromRank(rank);
	}

	private int determinePointsFromRank(Rank theRank) {
		switch(theRank.getRank()) {
			case TWO : 
				return 25;
			case THREE:
				return 3;
			case FOUR:
				return 4;
			case FIVE:
				return 5;
			case SIX:
				return 6;
			case SEVEN:
				return 7;
			case EIGHT:
				return 8;
			case NINE:
				return 9;
			case TEN:
				return 10;
			case JACK:
				return 10;
			case QUEEN:
				return 10;
			case KING:
				return 10;
			case ACE:
				return 20;
			case JOKER:
				return 50;
			default:
				return 0;
		}
	}

	public static Card jokerCard() {
		Card joker = new Card(new WildSuit(), Rank.buildJokerRank());
		return joker;		
	}

	public String getSymbolName() {
		return rank.getSymbol() + suit.getSymbol();
	}
	
	public int getPointsValue() {
		return points;
	}
	
	@Override
	public String toString() {
		return name;
	}

}
