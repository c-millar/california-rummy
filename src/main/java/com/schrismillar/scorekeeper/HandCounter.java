package com.schrismillar.scorekeeper;

import java.util.List;

import com.schrismillar.cards.Card;
import com.schrismillar.game.hand.Hand;

public class HandCounter {

	public int countHand(Hand hand) {
		return sumCards(hand.getCardsInHand());
	}

	private int sumCards(List<Card> cardsInHand) {
		int sum = 0;
		
		for(Card card : cardsInHand) {
			sum += card.getPointsValue();
		}
		
		return sum;
	}

}
