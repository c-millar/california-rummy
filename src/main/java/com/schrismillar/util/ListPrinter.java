package com.schrismillar.util;

import java.io.OutputStream;
import java.io.PrintStream;
import java.util.List;

public class ListPrinter {
	private PrintStream outputDestination;
	
	private ListPrinter(PrintStream outputDestination) {
		this.outputDestination = outputDestination;
	}
	
	public static ListPrinter getDefaultListPrinter() {
		return new ListPrinter(System.out);
	}
	
	public static ListPrinter getListPrinter(PrintStream outputDestination) {
		return new ListPrinter(outputDestination);
	}
	
	public <T> void printList(List<T> list) {
		for (T element : list) {
			outputDestination.println(element.toString());
		}
	} 
}
